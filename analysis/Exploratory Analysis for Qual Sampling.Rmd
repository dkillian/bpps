---
title: "Exploratory Models for Qualitative Sampling"
author: "Jacob Patterson-Stein"
date: "2021-02-01"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

## Introduction

This page explores three themes for informing qualitative sampling, as well as general BPPS modelling and learning agenda research. The three themes are:

*	Governance
*	COVID-19
*	Security



```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/00 BPPS - workflowr prep.R")

```

### Model 1

Model 1 is our base model. It simply looks at our outcome of interest as a function of a thematic variable, treatment.


$$\hat{Y}_{i} = \beta_{0} + \beta_{1}treatment_{i} + e_{i}\  (1)$$

### Model 2
The second model we will fit to the data is the same as Model 1, but with the control variables listed above. In this model, *X* is a vector of covariates. 

$$\hat{Y}_{i} = \beta_{0} + \beta_{1}treatment_{i} +\beta_{2}X_{i} + e_{i}\  (2)$$

# Key questions


## Governance
Our first analysis will addressing the following question:

> Does perception of GIRoA affect perception of USAID?

We'll first look at the relationship between perception of GIRoA and perception of USAID. From there, we'll see how response to COVID-19 might influence this relationship, and finally add in key respondent characteristics. 

```{r, echo = FALSE, warning = FALSE}
m1 <- svyglm(B4_bin  ~ GovtPerf 
                      , design = svyrdat 
                      
                      )


m2 <- svyglm(B4_bin  ~ GovtPerf + info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
                      , design = svyrdat 
                      
                      )

m3 <- svyglm(B4_bin  ~ GovtPerf + info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
             + factor(province)
                      , design = svyrdat 
                      
                      )

# summary(svyglm(GovtPerf ~ USAID_branded_acts   
#                , design = svyrdat 
#                
# )
# )

# 
# summary(svyglm(B4_bin  ~ GovtPerf 
#                , design = svyrdat 
#                
# )
# )


sjPlot::tab_model(m1
                  , m2
                  , m3
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Perception of GIRoA"
                                  , "Survey information treatment"
                                  , "Female"
                                  , "University-level education"
                                  , "Madrasa education"
                                  , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
                                  , "Never married"
                                  , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
                                  , "Income (log)"
                                  , "Economic ladder classification"
                                  , "Asset index"
                                  , "Urban Locality"
                                  , "Ghazni"
                                  , "Nangarhar"
                                  , "Takhar"
                                  , "Balkh"
                                  , "Herat"
                                  , "Kandahar"
                                  )
                  , dv.labels=c("Model 1", "Model 2", "Model 3")
)

```

It looks like the relationship between perception of GIRoA and perception of USAID is strong. We can unpack this a bit more. 


```{r, echo = FALSE, warning = FALSE}

m4 <-svyglm(B4_bin  ~ GovtPerf + H2_bin
                      , design = svyrdat 
                      
                      )

m5 <- svyglm(B4_bin  ~ GovtPerf + D7_USAID
                      , design = svyrdat 
                      
                      )

m6 <- svyglm(B4_bin  ~ GovtPerf + D7_USAID + H2_bin + H3_bin
                      , design = svyrdat 
                      
                      )



sjPlot::tab_model(m4
                  , m5
                  , m6
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Perception of GIRoA"
                                  , "Perception of GIRoA's COVID response"
                                  , "Participation in USAID activities"
                                  , "Perception of USAID's COVID response"
                                  )
                  , dv.labels=c("Model 4", "Model 5", "Model 6")
)

```

OK, so it looks like there are other variables that we might want to include. 

Let's pull these into our model
```{r, echo = FALSE, warning = FALSE}

m7 <- svyglm(B4_bin  ~ GovtPerf + D7_USAID + H2_bin
             + info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
             + factor(province)
                      , design = svyrdat 
                      )


sjPlot::tab_model(m7
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Perception of GIRoA"
                                  , "Perception of GIRoA's COVID response"
                                  , "Participation in USAID activities"
                                  , "Survey information treatment"
                                  , "Female"
                                  , "University-level education"
                                  , "Madrasa education"
                                  , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
                                  , "Never married"
                                  , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
                                  , "Income (log)"
                                  , "Economic ladder classification"
                                  , "Asset index"
                                  , "Urban Locality"
                                  , "Ghazni"
                                  , "Nangarhar"
                                  , "Takhar"
                                  , "Balkh"
                                  , "Herat"
                                  , "Kandahar"
                                  )
                  , dv.labels=c("Model 7")
)

```

Finally, let's just look at COVID response and GIRoA. 

```{r, echo = FALSE, warning = FALSE}

m8 <- svyglm(GovtPerf  ~  H2_bin

                      , design = svyrdat 
                      )


sjPlot::tab_model(m8
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Perception of GIRoA's COVID response"
                                  )
                  , dv.labels=c("Model 8")
)

```
## Security
Our second model follows a similar pattern, but with a focus on security provision. 

We will explore the following:

> Does perception of local security provision affect perception of USAID?

```{r, echo = FALSE, warning = FALSE}
sec.vars <- c("ANA_sect"
              , "natpolice_sect"
              , "local_police"
              , "police_arbakai"
              , "taliban"
              , "isis"
              , "age_sect"
              , "malik_sect"
              , "usaid_sect"
              , "nds_sect"
              , "top3_sect")


models <- list()

models <- lapply(sec.vars
                 , function(x) svyglm(formula(paste0("B4_bin ~", x)), design = svyrdat))

                                    


sjPlot::tab_model(models[[1]]
                  , models[[2]]
                  , models[[3]]
                  , models[[4]]
                  , models[[5]]
                  , models[[6]]
                  , models[[7]]
                  , models[[8]]
                  , models[[9]]
                  , models[[10]]
                  , models[[11]]
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "National Army"
                                  , "National Police"
                                  , "Local Police"
                                  , "Local Police/Arbakai"
                                  , "Taliban"
                                  , "ISIS"
                                  , "AGE"
                                  , "Village Malik"
                                  , "USAID"
                                  , "NDS"
                                  , "ANA, Natl. Police, Local Police"
                                  )
                  , dv.labels=c("Model 1", "Model 2", "Model 3", "Model 4", "Model 5", "Model 6", "Model 7", "Model 8", "Model 9", "Model 10", "Model 11")
)


```

It looks like ISIS, Taliban, and National Police security provision are correlated with perception of USAID. The presence of ISIS as the primary or secondary security provider appears to have a strongly negative correlation with perception of USAID. 

Let's see what happens if we control for our key covariates.

```{r, echo = FALSE, warning = FALSE}

models <- list()
models <- lapply(sec.vars
                 , function(x) svyglm((paste0("B4_bin ~ education + log(income) + madrassa + never_married + age_grpAf + asset_fac + ladder + uni + locality + female + factor(province) +", x)), design = svyrdat))



sjPlot::tab_model(models[[1]]
                  , models[[2]]
                  , models[[3]]
                  , models[[4]]
                  , models[[5]]
                  , models[[6]]
                  , models[[7]]
                  , models[[8]]
                  , models[[9]]
                  , models[[10]]
                  , models[[11]]
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
                                  , "Income (log)"
                                  , "Madrasa education"
                                  , "Never married"
                                  , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
                                  , "Asset index"
                                  , "Economic ladder classification"
                                  , "University-level education"
                                  , "Urban Locality"
                                  , "Female"
                                  , "Ghazni"
                                  , "Nangarhar"
                                  , "Takhar"
                                  , "Balkh"
                                  , "Herat"
                                  , "Kandahar"
                                  , "National Army"
                                  , "National Police"
                                  , "Local Police"
                                  , "Local Police/Arbakai"
                                  , "Taliban"
                                  , "ISIS"
                                  , "AGE"
                                  , "Village Malik"
                                  , "USAID"
                                  , "NDS"
                                  , "ANA, Natl. Police, Local Police"
                                  )
                  , dv.labels=c("Model 1", "Model 2", "Model 3", "Model 4", "Model 5", "Model 6", "Model 7", "Model 8", "Model 9", "Model 10", "Model 11")
)


```

### Sympathy for the Taliban

A key question is whether sympathy for the Taliban, separate from security provision, influences perception of USAID. This is particularly relevant given recent and on-going power sharing and peace agreements and discussions.

```{r, echo = FALSE, warning = FALSE}
m12 <- svyglm(B4_bin  ~ taliban_support 
                      , design = svyrdat 
                      
                      )

m13 <- svyglm(B4_bin  ~ taliban_support + taliban 
                      , design = svyrdat 
                      )

m14 <- svyglm(B4_bin  ~ taliban_support + GovtPerf 
                      , design = svyrdat 
                      )

m15 <- svyglm(B4_bin  ~ taliban_support + GovtPerf + taliban
                      , design = svyrdat 
                      )

m16 <- svyglm(B4_bin  ~ taliban_support + GovtPerf + taliban
                      , design = svyrdat 
                      )

m17 <- svyglm(B4_bin  ~ taliban_support + GovtPerf + taliban + info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
             # + factor(province) 
             , design = svyrdat 
                      )



m18 <- svyglm(B4_bin  ~ taliban_support + GovtPerf + taliban + info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
             + factor(province)
             , design = svyrdat 
                      )


sjPlot::tab_model(m12
                  , m13
                  , m14
                  , m15
                  , m16
                  , m17
                  , m18
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Support for Taliban"
                                  , "Taliban provides security"
                                  , "Perception of GIRoA"
                                  , "Survey information treatment"
                                  , "Female"
                                  , "University-level education"
                                  , "Madrasa education"
                                  , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
                                  , "Never married"
                                  , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
                                  , "Income (log)"
                                  , "Economic ladder classification"
                                  , "Asset index"
                                  , "Urban Locality"
                                  , "Ghazni"
                                  , "Nangarhar"
                                  , "Takhar"
                                  , "Balkh"
                                  , "Herat"
                                  , "Kandahar"
                                  )
                  , dv.labels=c("Model 12", "Model 13", "Model 14", "Model 15", "Model 16", "Model 17", "Model 18")
)

```

## COVID-19
Our final analysis will addressing the following question:

> Does perception of COVID-19 response affect perception of USAID?

We already looked at this in the governance section, but let's unpack COVID-19 and USAID perception a bit more.

```{r, echo = FALSE, warning = FALSE}

m19 <- svyglm(B4_bin  ~ H2_bin 
                      , design = svyrdat 
                      
                      )


m20 <- svyglm(B4_bin  ~ H3_bin
                      , design = svyrdat 
                      
                      )


m21 <- svyglm(B4_bin  ~ H2_bin + H3_bin
                      , design = svyrdat 
                      )


m22 <- svyglm(B4_bin  ~ H2_bin + H3_bin + cov_bin
                      , design = svyrdat 
                      )

m23 <- svyglm(B4_bin  ~ H2_bin + H3_bin + cov_bin + D7_USAID
                      , design = svyrdat 
                      )

m24 <- svyglm(B4_bin  ~  cov_bin 
                      , design = svyrdat 
                      )

m25 <- svyglm(B4_bin  ~ H2_bin + H3_bin + cov_bin + D7_USAID
              +  info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
                      , design = svyrdat 
                      )

m26 <- svyglm(B4_bin  ~ H2_bin + H3_bin + cov_bin + D7_USAID
              +  info
             + female
             + uni
             + madrassa
             + education
             + never_married
             + age_grpAf
             + log(income)
             + ladder
             + asset_fac
             + locality 
             + factor(province)
                      , design = svyrdat 
                      )



sjPlot::tab_model(m19
                  , m20
                  , m21
                  , m22
                  , m23
                  , m24
                  , m25
                  , m26
                  , show.ci=.95
                  , show.aic=TRUE
                  , show.loglik=T
                  , auto.label = F
                  # , robust = TRUE
                  # , vcov.type = "HC1"
                  , collapse.ci=T
                  , show.intercept = T
                  , pred.labels=c("Intercept"
                                  , "Perception of GIRoA's COVID response"
                                  , "Perception of USAID's COVID response"
                                  , "Experienced any COVID-19 symptoms"
                                  , "Participation in USAID activities"
                                  , "Survey information treatment"
                                  , "Female"
                                  , "University-level education"
                                  , "Madrasa education"
                                  , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
                                  , "Never married"
                                  , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
                                  , "Income (log)"
                                  , "Economic ladder classification"
                                  , "Asset index"
                                  , "Urban Locality"
                                  , "Ghazni"
                                  , "Nangarhar"
                                  , "Takhar"
                                  , "Balkh"
                                  , "Herat"
                                  , "Kandahar"
                                  )
                  , dv.labels=c("Model 19", "Model 20", "Model 21", "Model 22", "Model 23", "Model 24", "Model 25")
)

```


It looks like one people who have had COVID-19 symptoms are more likely to have a positive perception of USAID, even while accounting for broader perception of USAID's support of COVID-19 response.

## Tree models

The above tables provides an overview of the relationships between our three themes and perception of USAID. We also want to better understand various subsets of our data. This section briefly highlights a few tree-based model outputs for our key themes.

### Governance

We will first look at perception of USAID as a function of perception of GIRoA performance, participation in USAID activities, and perception of USAID's support for COVID-19 response.

```{r, echo = FALSE, warning = FALSE}
library(party)

dat2 <- subset(dat, !is.na(B4_bin)) %>% 
  select("Perception of USAID" = B4_bin
         , "Perception of GIRoA" = GovtPerf
         , "USAID activity participation" = D7_USAID
         , "Perception of USAID's COVID-19 response" = H3_bin
         ) %>% 
  mutate_all(as.numeric)

t1 <- ctree(`Perception of USAID`  ~ .
                      , data = dat2 
             , controls = ctree_control(
    mincriterion=.80
  )
                      )

# plot(t1)
# 
# plot(t1,
# inner_panel=node_inner(c, pval=F),
# terminal_panel=node_terminal(c,
# abbreviate=T,
# digits=2,
# fill="white"))

```


### Security
```{r, echo = FALSE, warning = FALSE}

dat3 <- subset(dat, !is.na(B4_bin)) %>% 
  select("Perception of USAID" = B4_bin
         , "Perception of GIRoA" = GovtPerf
         , "Support Taliban" = taliban_support
) %>% 
  mutate_all(as.numeric)

t2 <- ctree(`Perception of USAID`  ~ .
                      , data = dat3 
             , controls = ctree_control(
    mincriterion=.80
  )
                      )

plot(t2)

```


### COVID-19
```{r, echo = FALSE, warning = FALSE}

dat3 <- subset(dat, !is.na(B4_bin)) %>% 
  select("Perception of USAID" = B4_bin
         , "Perception of USAID's COVID-19 response" = H3_bin
         , "Perception of GIRoA's COVID-19 response" = H3_bin
         , "COVID-19 Symptoms" =  cov_bin
         , "USAID activity participation" = D7_USAID

) %>% 
  mutate_all(as.numeric)

t3 <- ctree(`Perception of USAID`  ~ .
                      , data = dat3 
             , controls = ctree_control(
    mincriterion=.80
  )
                      )

plot(t3)

```


### Sampler platter
```{r, echo = FALSE, warning = FALSE}

dat4 <- subset(dat, !is.na(B4_bin)) %>% 
  mutate(income = log(income)
         , GovtPerf = as.numeric(GovtPerf)
         , asset_fac = as.numeric(asset_fac)
         , ladder = as.numeric(ladder)
         , locality = factor(locality, levels = c("Urban", "Rural"))
         ) %>% 
  select("Perception of USAID" = B4_bin
         , "Perception of USAID's COVID-19 response" = H3_bin
         , "Perception of GIRoA's COVID-19 response" = H3_bin
         # , "COVID-19 Symptoms" =  cov_bin
         , "USAID activity participation" = D7_USAID
         , "Perception of GIRoA" = GovtPerf
         , "Support Taliban" = taliban_support
         , "Urban" = locality
         , "Female" = female
         , "Education Level" = education
         , "Age Group" = age_grpAf
         , "Income" = income
         , "Ladder" =  ladder
         , "Asset Index" =  asset_fac

) 

t4 <- ctree(`Perception of USAID`  ~ .
                      , data = dat4
             
                      )

plot(t4)

```
