---
title: "All treatments"
author: "Dan Killian"
date: "2021-02-01"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/00 BPPS - workflowr prep.R")

```

### Introduction

This section summarizes the regression models that include all treatment variables. 

### Full sample

```{r}

svybase <- svyrdat %>%
  select(aware
         , USAID_perception_bin
         , USAID_perception_ord
         , GovtPerf
         , USAID_info
         , USAID_message
         , USAID_branded_act
         , USAID_branded_acts
         , USAID_participation
         , hrs_tv
         , hrs_radio
         , wk_hrs_tv
         , wk_hrs_radio
         , activities
         , female
         , uni
         , madrassa
         , education
         , never_married
         , age
         , age_grpAf
         , log_inc
         , ladder
         , asset_fac
         , locality
         , province
         , strata
         , ethnicity
         , corruption
         , corruption_bin
         , bribe
         , violence_victim
         , unsafe
         , ANA_sect
         , natpolice_sect
         , isis
         , pashtun
         , tajik
         , urban
         , local_police
         , age_sect
         , malik_sect
         , usaid_sect
         ) %>%
  na.omit()
```

#### Linear probability model

```{r}
all1 <- svyglm(USAID_perception_bin ~ info, 
             design=svydat)

summary(all1)
```

```{r}
all2 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message, 
             design=svydat)

summary(all2)
```

```{r}
all3 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act, 
             design=svydat)

summary(all3)
```

```{r}
all4 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation, 
             design=svybase)

summary(all4)
```

```{r}
tab_model(all4,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```

```{r}
all5 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + as.numeric(corruption) + bribe, 
             design=svybase)

#summary(all5)
```

```{r}
tab_model(all5,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```


```{r}
all6 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

#summary(all6)
```

```{r}
all7 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities +  as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(all7)
```

```{r}

all7_out <- tidy_out(all7,
                     term_key=term_key2)

all7_out

```

```{r}
regplot(data=all7_out, 
        xmin = -.3,
        xmax = .2, 
        limits = c(-.31, .21))

ggsave("output/reg plots/perception omnibus regplot model 7 full.png",
       type="cairo",
       device="png",
       height=8.5,
       width=10)
```


```{r}
all7_out_filt <- all7_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
all7_out_filt
```

```{r}
regplot(data=all7_out_filt, 
        xmin = -.3,
        xmax = .15, 
        limits = c(-.307, .1822))

ggsave("output/reg plots/perception omnibus regplot model 7 filt.png",
       type="cairo",
       device="png",
       height=4,
       width=7.5)
```

```{r}
all7_labs <- all7_out$term_lab
```

```{r}
tab_model(all4, all5, all6, all7,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          pred.labels = all7_labs,
          wrap.labels=15,
          dv.labels=c("Treatments only","Base controls","Full controls","Ethnicity, province\nfixed effects"),
          title="USAID perception, linear probability model",
          file="output/reg tables/all treatments, lpm.doc")
```

```{r}
all8 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities +  as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(all8)
```

```{r}

all8_out <- tidy_out(all8,
                     term_key=term_key2)

all8_out

```

```{r}
regplot(data=all8_out, 
        xmin = -.3,
        xmax = .2, 
        limits = c(-.31, .21))

ggsave("output/reg plots/perception omnibus regplot model 8 full.png",
       type="cairo",
       device="png",
       height=8.5,
       width=10)
```


```{r}
all8_out_filt <- all8_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
all8_out_filt
```

```{r}
regplot(data=all8_out_filt, 
        xmin = 0,
        xmax = .15, 
        limits = c(0, .15))

ggsave("output/reg plots/perception omnibus regplot model 8 filt.png",
       type="cairo",
       device="png",
       height=4,
       width=7.5)
```

```{r}
all7_labs <- all7_out$term_lab
```




```{r}

tab_model(a4, a5, a7
          , show.intercept=F
          , collapse.ci=T
          , show.p=F
          , show.aic=TRUE
          , show.loglik=T)
# 
#                   , auto.label = F
#                   # , robust = TRUE
#                   # , vcov.type = "HC1"
#                   , collapse.ci=T
#                   , show.intercept = T
#                   , pred.labels=c("Intercept"
#                                   , "Female"
#                                   , "University-level education"
#                                   , "Madrasa education"
#                                   , "Overall education level (1=no ed, 2 = less than high school, 3 = high school+"
#                                   , "Never married"
#                                   , "Age group (1 = 18-35, 2 = 35-54, 3 = 55+"
#                                   , "Income (log)"
#                                   , "Economic ladder classification"
#                                   , "Asset index"
#                                   , "Urban Locality"
#                                   , "Perception of GIRoA"
#                                   , "Ghazni"
#                                   , "Nangarhar"
#                                   , "Takhar"
#                                   , "Balkh"
#                                   , "Herat"
#                                   , "Kandahar"
#                                   , "Security Situation Changed"
#                                   , "Violence Victim"
#                                   )
#                   , dv.labels=c("Model 1","Model 2", "Model 3", "Model 4", "Model 5")
# )
# 

```

#### Common sample

```{r}

svybase <- svyrdat %>%
  select(aware
         , USAID_perception_bin
         , USAID_perception_ord
         , GovtPerf
         , USAID_info
         , USAID_message
         , USAID_branded_act
         , USAID_branded_acts
         , USAID_participation
         , activities
         , female
          , uni
          , madrassa
          , education
          , never_married
          , age_grpAf
          , log_inc
          , ladder
          , asset_fac
          , locality
          , province
         , strata
         , ethnicity
          , violence_victim
         , unsafe
          , ANA_sect
          , natpolice_sect
          , isis
          , pashtun
          , tajik
          , urban
          , local_police
          , age_sect
          , malik_sect
          , usaid_sect
         ) %>%
  na.omit()
```

```{r}
a1 <- svyglm(USAID_perception_bin ~ USAID_info, 
             design=svybase)

summary(a1)
```

```{r}
a1.1 <- svyglm(USAID_perception_bin ~ USAID_info + aware + USAID_info*aware, 
             design=svybase)

summary(a1.1)
```


```{r}
a2 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message, 
             design=svybase)

summary(a2)
```

```{r}
a3 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act, 
             design=svybase)

summary(a3)
```

```{r}
a4 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation, 
             design=svybase)

summary(a4)
```

```{r}
tab_model(a4,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```

```{r}
a4.1 <- svyglm(USAID_perception_bin ~ USAID_info*aware + USAID_message + USAID_branded_act + USAID_participation, 
             design=svybase)

summary(a4.1)
```

```{r}
a5 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + 
               locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac, 
             design=svybase)

summary(a5)
```

```{r}
tab_model(a5,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```


```{r}
a6 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + 
               locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + 
               as.factor(province) + as.factor(ethnicity), 
             design=svybase)

summary(a6)
```

```{r}
a7 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + 
               locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + 
               as.factor(province) + as.factor(ethnicity) + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

summary(a7)
```

```{r}
tab_model(a7,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```

```{r}
a8 <- svyglm(USAID_perception_bin ~ locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + 
               as.factor(province) + as.factor(ethnicity) + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + USAID_info + USAID_info:aware + USAID_message + USAID_message:aware + USAID_branded_act + USAID_branded_act:aware + USAID_participation + USAID_participation:aware, 
             design=svybase)

summary(a8)
```

```{r}

tab_model(a4, a5, a7
          , show.intercept=F
          , collapse.ci=T
          , show.p=F
          , show.aic=TRUE
          , show.loglik=T
          , dv.labels=c("Treatment effects","Base controls", "Full controls")
          , title="USAID perception, all treatments, linear probability model"
          , file="output/reg tables/all treatments lpm.doc")

```

### Classification and regression trees

#### USAID perception, no treatment

```{r}
dat2 <- dat %>%
  select(#aware
         , USAID_perception_bin
         #, USAID_perception_ord
         , GovtPerf
         #, USAID_info
         #, USAID_message
         #, USAID_branded_act
         #, USAID_branded_acts
         #, USAID_participation
         , female
         , uni
         , madrassa
         , education
         , never_married
         , age_grpAf
         , log_inc
         , ladder
         , asset_fac
         #, province
         #, strata
         , ethnicity
         , violence_victim
         , unsafe
         , ANA_sect
         , natpolice_sect
         , isis
         , pashtun
         , tajik
         , urban
         , local_police
         , age_sect
         , malik_sect
         , usaid_sect
  ) %>%
  na.omit()
```

```{r fig.width=10}
library(party)
library(rpart)
library(rattle)
library(randomForest)

str(dat)
lapply(dat2, class)

perception_ct <- ctree(as.factor(USAID_perception_bin) ~ .,
                  controls=ctree_control(mincriterion=.95,
                                         minsplit = 40,
                                         minbucket = 400),
               data=dat2)

plot(perception_ct, type="simple")

```

#### USAID perception, with treatments

```{r}
dat3 <- dat %>%
  select(aware
         , USAID_perception_bin
         #, USAID_perception_ord
         , GovtPerf
         , USAID_info
         , USAID_message
         , USAID_branded_act
         #, USAID_branded_acts
         , USAID_participation
         , female
         , uni
         , madrassa
         , education
         , never_married
         , age_grpAf
         , log_inc
         , ladder
         , asset_fac
         #, province
         #, strata
         , ethnicity
         , violence_victim
         , unsafe
         , ANA_sect
         , natpolice_sect
         , isis
         , pashtun
         , tajik
         , urban
         , local_police
         , age_sect
         , malik_sect
         , usaid_sect
  ) %>%
  #filter(aware==1) %>%
  na.omit()
```

```{r fig.width=10}

perception_treat_ct <- ctree(as.factor(USAID_perception_bin) ~ .,
                  controls=ctree_control(mincriterion=.95,
                                         minsplit = 30,
                                         minbucket = 300),
               data=dat3)

plot(perception_treat_ct, type="simple")

```

### Aware only

```{r}
a1 <- svyglm(aware ~ locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe,
             design=svybase)

summary(a1)
```

```{r}
plotreg(a1,
        omit.coef = "(Intercept)")
```


```{r}
a2 <- svyglm(aware ~ locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

summary(a2)
```

```{r}
plotreg(a2,
        omit.coef = "(Intercept)")
```

```{r}
a2_out <- tidy(a2) %>%
  mutate(lower = estimate - 1.96*std.error,
         upper = estimate + 1.96*std.error,
         color=case_when(lower > 0 ~ "#002F6C",
                         upper < 0 ~ "#BA0C2F",
                         TRUE ~ "#8C8985"), 
         term_lab = c("Intercept","Urban resident","Female","Level of education (1-3)", "Madrassa education",
                      "Never married","Age group (1-3)","Log income","Asset score", "Activities in community", "Daily hours TV", "Daily hours radio use", "Problem of corruption", "Solicited to pay bribe","Victim of violence","Feel unsafe in community","ANA security","National police security","Local police security","GIRoA performance")) %>%
  filter(term_lab != "Intercept")

a2_out

```


```{r}

regplot(data=a2_out, 
        xmin = -.15,
        xmax = .1, 
        limits = c(-.163, .1))

ggsave("output/reg plots/awareness model 2 full.png",
       device="png",
       type="cairo",
       height=5,
       width=7)

```

```{r}

a2_out_filt <- a2_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")

a2_out_filt

```

```{r}

regplot(data=a2_out_filt, 
        xmin = -.15,
        xmax = .1, 
        limits = c(-.163, .1))

```


```{r}
a3 <- svyglm(aware ~ locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(a3)
```

```{r}
plotreg(a3,
        omit.coef = "(Intercept)")
```

```{r}
# a3_out <- tidy(a3) %>%
#   mutate(lower = estimate - 1.96*std.error,
#          upper = estimate + 1.96*std.error,
#          color=case_when(lower > 0 ~ "#002F6C",
#                          upper < 0 ~ "#BA0C2F",
#                          TRUE ~ "#8C8985"), 
#          term_lab = c("Intercept","Urban resident","Female","Level of education (1-3)", "Madrassa education",
#                       "Never married","Age group (1-3)","Log income","Asset score", "Activities in community", "Daily hours TV", "Daily hours radio use", "Problem of corruption", "Solicited to pay bribe","Victim of violence","Feel unsafe in community","ANA security","National police security","Local police security","GIRoA performance", "Tajik","Uzbek","Turkmen","Hazara","Arab","Other ethnicity")) %>%
#   filter(term_lab != "Intercept")

a3_out <- tidy_out(a3,
                   term_key = term_key2)

a3_out

```


```{r}

regplot(data=a3_out, 
        xmin = -.15,
        xmax = .25, 
        limits = c(-.19, .3))

ggsave("output/reg plots/awareness model 3 full.png",
       device="png",
       type="cairo",
       height=7,
       width=9)

```

```{r}

a3_out_filt <- a3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")

a3_out_filt

```

```{r}

regplot(data=a3_out_filt, 
        xmin = -.15,
        xmax = .1, 
        limits = c(-.15, .1))

ggsave("output/reg plots/awareness model 3 filt.png",
       device="png",
       type="cairo",
       height=4,
       width=7)

```

```{r}
a4 <- svyglm(aware ~ locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf +  as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(a4)
```

```{r}
a4_out <- tidy_out(a4,
                    term_key = term_key2)

a4_labs <- a4_out$term_lab

```


```{r}
plotreg(a4,
        omit.coef = "(Intercept)")
```

```{r}
tab_model(a1, a2, a3, a4,
          show.intercept=F,
          show.p=F,
          collapse.ci=T,
          pred.labels = a4_labs,
          dv.labels = c("Base controls","Full controls","Ethnicity fixed effects", "Province fixed effects"),
          file="output/reg tables/awareness.doc")
```



### USAID messaging only

```{r}
mes1 <- svyglm(USAID_perception_bin ~ USAID_message + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe, 
             design=svybase)

#summary(mes1)
```

```{r}
plotreg(mes1,
        omit.coef = "(Intercept)")
```


```{r}
mes2 <- svyglm(USAID_perception_bin ~ USAID_message + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

#summary(mes2)
```

```{r}
plotreg(mes2,
        omit.coef = "(Intercept)")
```

```{r}
mes3 <- svyglm(USAID_perception_bin ~ USAID_message + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(mes3)
```

```{r}
plotreg(mes3,
        omit.coef = "(Intercept)")
```

```{r}

mes3_out <- tidy_out(mes3,
                     term_key=term_key2)

mes3_out

```

```{r}
regplot(data=mes3_out, 
        xmin = -.3,
        xmax = .15, 
        limits = c(-.35, .165))

ggsave("output/reg plots/message perception regplot model 3 full.png",
       type="cairo",
       device="png",
       height=6.5,
       width=8)
```


```{r}
mes3_out_filt <- mes3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
mes3_out_filt
```

```{r}
regplot(data=mes3_out_filt, 
        xmin = 0,
        xmax = .15, 
        limits = c(-.01, .17))

ggsave("output/reg plots/message perception regplot model 3 filt.png",
       type="cairo",
       device="png",
       height=3.5,
       width=6)
```

```{r}
mes4 <- svyglm(USAID_perception_bin ~ USAID_message + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf +  as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(mes4)
```

```{r}
plotreg(mes4,
        omit.coef = "(Intercept)")
```

```{r}

mes4_out <- tidy_out(mes4,
                     term_key=term_key2)

mes4_out
mes4_labs <- mes4_out$term_lab

```


```{r}
tab_model(mes1, mes2, mes3,mes4,
          show.intercept=F,
          show.p=F,
          collapse.ci=T
          , pred.labels = mes4_labs
          , dv.labels = c("Base controls","Full controls","Ethnicity fixed effects", "Province fixed effects")
          , file="output/reg tables/media.doc")
```



### USAID activities only

```{r}
act1 <- svyglm(USAID_perception_bin ~ USAID_branded_act + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe, 
             design=svybase)

#summary(mes1)
```

```{r}
plotreg(act1,
        omit.coef = "(Intercept)")
```


```{r}
act2 <- svyglm(USAID_perception_bin ~ USAID_branded_act + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

#summary(mes2)
```

```{r}
plotreg(act2,
        omit.coef = "(Intercept)")
```

```{r}
act3 <- svyglm(USAID_perception_bin ~ USAID_branded_act + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(act3)
```

```{r}
plotreg(act3,
        omit.coef = "(Intercept)")
```

```{r}

act3_out <- tidy_out(act3,
                     term_key=term_key2)

act3_out

```

```{r}
regplot(data=act3_out, 
        xmin = -.3,
        xmax = .15, 
        limits = c(-.35, .167))

ggsave("output/reg plots/activity perception regplot model 3 full.png",
       type="cairo",
       device="png",
       height=7,
       width=8)
```


```{r}
act3_out_filt <- act3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
act3_out_filt
```

```{r}
regplot(data=act3_out_filt, 
        xmin = 0,
        xmax = .15, 
        limits = c(0, .15))

ggsave("output/reg plots/activity perception regplot model 3 filt.png",
       type="cairo",
       device="png",
       height=3.5,
       width=6)
```

```{r}
act4 <- svyglm(USAID_perception_bin ~ USAID_branded_act + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(act4)
```

```{r}
plotreg(act4,
        omit.coef = "(Intercept)")
```

```{r}
act4_out <- tidy_out(act4,
                     term_key=term_key2)

act4_labs <- act4_out$term_lab

```


```{r}
tab_model(act1, act2, act3,act4,
          show.intercept=F,
          show.p=F,
          collapse.ci=T
          , pred.labels = act4_labs
          , dv.labels = c("Base controls","Full controls","Ethnicity fixed effects", "Province fixed effects")
          , file="output/reg tables/activity exposure.doc")
```

### USAID participation only

```{r}
prt1 <- svyglm(USAID_perception_bin ~ USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe, 
             design=svybase)

#summary(mes1)
```

```{r}
plotreg(prt1,
        omit.coef = "(Intercept)")
```


```{r}
prt2 <- svyglm(USAID_perception_bin ~ USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

#summary(mes2)
```

```{r}
plotreg(prt2,
        omit.coef = "(Intercept)")
```

```{r}
prt3 <- svyglm(USAID_perception_bin ~ USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(prt3)
```

```{r}
plotreg(prt3,
        omit.coef = "(Intercept)")
```

```{r}

prt3_out <- tidy_out(prt3,
                     term_key=term_key2)

prt3_out

```

```{r}
regplot(data=prt3_out, 
        xmin = -.35,
        xmax = .25, 
        limits = c(-.37, .25))

ggsave("output/reg plots/participation perception regplot model 3 full.png",
       type="cairo",
       device="png",
       height=7,
       width=8)
```


```{r}
prt3_out_filt <- prt3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
prt3_out_filt
```

```{r}
regplot(data=prt3_out_filt, 
        xmin = 0,
        xmax = .25, 
        limits = c(0, .25))

ggsave("output/reg plots/participation perception regplot model 3 filt.png",
       type="cairo",
       device="png",
       height=3.5,
       width=6)
```

```{r}
prt4 <- svyglm(USAID_perception_bin ~ USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(prt4)
```

```{r}
plotreg(prt4,
        omit.coef = "(Intercept)")
```

```{r}
prt4_out <- tidy_out(prt4,
                     term_key=term_key2)

prt4_labs <- prt4_out$term_lab

```


```{r}
tab_model(prt1, prt2, prt3,prt4,
          show.intercept=F,
          show.p=F,
          collapse.ci=T
          , pred.labels = prt4_labs
          , dv.labels = c("Base controls","Full controls","Ethnicity fixed effects", "Province fixed effects")
          , file="output/reg tables/activity participation.doc")
```



### USAID info only

```{r}
inf1 <- svyglm(USAID_perception_bin ~ USAID_info + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe, 
             design=svybase)

summary(inf1)
```

```{r}
plotreg(inf1,
        omit.coef = "(Intercept)")
```


```{r}
inf2 <- svyglm(USAID_perception_bin ~ USAID_info + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf, 
             design=svybase)

#summary(mes2)
```

```{r}
plotreg(inf2,
        omit.coef = "(Intercept)")
```

```{r}
inf3 <- svyglm(USAID_perception_bin ~ USAID_info + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(inf3)
```

```{r}
plotreg(inf3,
        omit.coef = "(Intercept)")
```

```{r}

inf3_out <- tidy_out(inf3,
                     term_key=term_key2)

inf3_out

```

```{r}
regplot(data=inf3_out, 
        xmin = -.35,
        xmax = .15, 
        limits = c(-.38, .15))

ggsave("output/reg plots/information perception regplot model 3 full.png",
       type="cairo",
       device="png",
       height=7,
       width=8)
```


```{r}
inf3_out_filt <- inf3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
inf3_out_filt
```

```{r}
regplot(data=inf3_out_filt, 
        xmin = 0,
        xmax = .1, 
        limits = c(0, .1))

ggsave("output/reg plots/information perception regplot model 3 filt.png",
       type="cairo",
       device="png",
       height=3.5,
       width=6)
```

```{r}
inf4 <- svyglm(USAID_perception_bin ~ USAID_info + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + hrs_tv + hrs_radio + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity) + as.factor(province), 
             design=svybase)

#summary(inf4)
```

```{r}
plotreg(inf4,
        omit.coef = "(Intercept)")
```

```{r}
inf4_out <- tidy_out(inf4,
                     term_key=term_key2)

inf4_labs <- inf4_out$term_lab

```


```{r}
tab_model(inf1, inf2, inf3,inf4,
          show.intercept=F,
          show.p=F,
          collapse.ci=T
          , pred.labels = inf4_labs
          , dv.labels = c("Base controls","Full controls","Ethnicity fixed effects", "Province fixed effects")
          , file="output/reg tables/info.doc")
```

### GIRoA Performance

```{r}
gov1 <- svyglm(GovtPerf ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation, 
             design=svybase)

#summary(all4)
```

```{r}
tab_model(all4,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```

```{r}
gov2 <- svyglm(GovtPerf ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + as.numeric(corruption) + bribe, 
             design=svybase)

#summary(all5)
```

```{r}
tab_model(all5,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          dv.labels="Linear probability model",
          title="USAID perception")
```


```{r}
gov3 <- svyglm(GovtPerf ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities + as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police, 
             design=svybase)

#summary(all6)
```

```{r}
gov4 <- svyglm(GovtPerf ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities +  as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + as.factor(ethnicity) + as.factor(province), 
             design=svybase)

summary(all7)
```

```{r}

gov3_out <- tidy_out(gov3,
                     term_key=term_key2)

gov3_out

```

```{r}
regplot(data=gov3_out, 
        xmin = -.25,
        xmax = .25, 
        limits = c(-.25, .25))

ggplot(gov3_out, aes(x=estimate, y=fct_reorder(term_lab, estimate)), color = color, fill = color) +
    geom_vline(xintercept=0, color="darkgrey", size=1.2) +
    geom_errorbarh(aes(xmin=lower, xmax=upper), color=gov3_out$color, height=0, size=1.2, alpha = 0.7) +
    geom_label(aes(label=round(estimate,2)),color = gov3_out$color, size=4.5) +
    scale_x_continuous(breaks=seq(-.35, .25, .1),
                       limits=c(-.34,.25)) +
    labs(x = "", y = "")


ggsave("output/reg plots/GIRoA perception omnibus regplot model 3 full.png",
       type="cairo",
       device="png",
       height=8.5,
       width=10)
```


```{r}
gov3_out_filt <- gov3_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
gov3_out_filt
```

```{r}
a <- regplot(data=gov3_out_filt, 
        xmin = -.25,
        xmax = .25, 
        limits = c(-.25, .25))

a + 
  scale_x_continuous(breaks=seq(-.25,.25,.1),
                     labels=percent_format(accuracy=1)) + 
  geom_label(aes(label=round(estimate,2), color = gov3_out$color, size=4.5))

ggplot(gov3_out_filt, aes(x=estimate, y=fct_reorder(term_lab, estimate)), color = color, fill = color) +
    geom_vline(xintercept=0, color="darkgrey", size=1.2) +
    geom_errorbarh(aes(xmin=lower, xmax=upper), color=gov3_out_filt$color, height=0, size=1.2, alpha = 0.7) +
    geom_label(aes(label=round(estimate,2)),color = gov3_out_filt$color, size=4.5) +
    scale_x_continuous(breaks=seq(-.25, .25, .1),
                       limits=c(-.27,.25)) +
    labs(x = "", y = "")



ggsave("output/reg plots/GIRoA perception omnibus regplot model 3 filt.png",
       type="cairo",
       device="png",
       height=4,
       width=7.5)
```

```{r}
gov4_out <- tidy_out(gov4, term_key=term_key2)
gov4_labs <- gov4_out$term_lab
```

```{r}
tab_model(gov1, gov2, gov3, gov4,
          show.intercept=F,
          collapse.ci = T,
          show.p = F,
          pred.labels = gov4_labs,
          wrap.labels=15,
          dv.labels=c("Treatments only","Base controls","Full controls","Ethnicity, province\nfixed effects"),
          title="USAID perception, linear probability model",
          file="output/reg tables/GIRoA performance, lpm.doc")
```

```{r}
all8 <- svyglm(USAID_perception_bin ~ USAID_info + USAID_message + USAID_branded_act + USAID_participation + locality + female + education + madrassa + never_married + age_grpAf + log_inc + asset_fac + activities +  as.numeric(corruption) + bribe + violence_victim + unsafe + ANA_sect + natpolice_sect + local_police + GovtPerf + as.factor(ethnicity), 
             design=svybase)

summary(all8)
```

```{r}

all8_out <- tidy_out(all8,
                     term_key=term_key2)

all8_out

```

```{r}
regplot(data=all8_out, 
        xmin = -.3,
        xmax = .2, 
        limits = c(-.31, .21))

ggsave("output/reg plots/perception omnibus regplot model 8 full.png",
       type="cairo",
       device="png",
       height=8.5,
       width=10)
```


```{r}
all8_out_filt <- all8_out %>%
  filter(color=="#BA0C2F" | color=="#002F6C")
all8_out_filt
```

```{r}
regplot(data=all8_out_filt, 
        xmin = 0,
        xmax = .15, 
        limits = c(0, .15))

ggsave("output/reg plots/perception omnibus regplot model 8 filt.png",
       type="cairo",
       device="png",
       height=4,
       width=7.5)
```
