---
title: "Thursday Meeting Models"
author: "Jacob Patterson-Stein"
date: "2/15/2021"
output: html_document
---
author: "dkillian"
date: "2021-02-01"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

## Introduction
This page presents inferential findings to understand the relationship between key treatment indicators and perception of USAID. 

Before we start any modelling, it's helpful to get a sense of the average values for our key covariates and the main outcome of interest, perception of USAID.

We have the following treatments:

- Participation in USAID activities
- Survey information treatment 
- Exposure to USAID activities
- Exposure to USAID messaging

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/00 BPPS - workflowr prep.R")


```

```{r, echo = FALSE, warning = FALSE}

multi.func <- function(var, label){
  svyrdat %>%
    group_by({{var}}) %>%
    summarise(proportion = survey_mean( )) %>%
    # slice(-1) %>%
    rename("Var" =1) %>%
    filter(is.na(Var)!=T) %>% 
    mutate(Variable = {{label}})
}


multi.func(info, "Information Treatment") %>% 
  bind_rows(multi.func(D7_USAID , "Participated in any USAID activities")) %>% 
  bind_rows(svyrdat %>% summarise(proportion = survey_mean(activities, na.rm=T))  %>% mutate(Variable = "Number of USAID activities", Var = 1) %>% select(Var, Variable, proportion, proportion_se)) %>% 
  bind_rows(multi.func(USAID_message , "Exposed to USAID Messaging"))%>% 
  filter(Var!=0) %>%
  mutate( "Margin of Error" = proportion_se * 1.96) %>% 
  select(Variable
         , "Percentage" = proportion
         , "Margin of Error") %>% 
  gt() %>% 
  fmt_percent(columns = c(2, 3), rows = c(1:2, 4),  decimals = 1) %>% 
  fmt_number(columns = c(2,3), rows = 3, decimals = 1) %>% 
  tab_header(title = "Treatment Indicators")
```


### Model 1

Model 1 is our base model. It simply looks at our outcome of interest as a function of a thematic variable, treatment.

$$\hat{Perception of USAID}_{i} = \beta_{0} + \beta_{1}treatment_{i} + e_{i}\  (1)$$

While we do want to control for other variables, such as respondent education and gender, this basic model provides a helpful estimate of the general direction and magnitude of the relationship between our treatments and our key outcome, perception of USAID. 

```{r, echo = FALSE}

m1 <- svyglm(B4_bin  ~ info 
                      , design = svyrdat 
                      )

m2 <- svyglm(B4_bin  ~ D7_USAID 
                      , design = svyrdat 
                      )

m3 <- svyglm(B4_bin  ~ activities
                      , design = svyrdat 
                      )

m4 <- svyglm(B4_bin  ~ USAID_message
                      , design = svyrdat 
                      )



# sjPlot::tab_model(m1
#                   , m2
#                   , m3
#                   , m4
#                   , show.p = FALSE
#                   , show.ci=.95
#                   , show.aic=TRUE
#                   , show.loglik=T
#                   , auto.label = F
#                   # , robust = TRUE
#                   # , vcov.type = "HC1"
#                   , collapse.ci=T
#                   , show.intercept = T
#                   , pred.labels=c("Intercept"
#                                   , "Survey information treatment"
#                                   , "Participated in USAID activities (yes/no)"
#                                   , "Number of USAID activities"
#                                   , "Exposed to USAID messaging")
#                   , dv.labels = c("Perception of USAID", "Perception of USAID", "Perception of USAID", "Perception of USAID")
# )



```


```{r, echo = FALSE, warning = FALSE}


bi.tidy.ords <- tidy(m1) %>% filter(term == "info") %>% 
  bind_rows(tidy(m2) %>% filter(term == "D7_USAID") ) %>% 
 bind_rows(tidy(m3) %>% filter(term == "activities") ) %>% 
 bind_rows(tidy(m4) %>% filter(term == "USAID_message") ) %>% 
  mutate(lower = estimate - 1.96*std.error,
         upper = estimate + 1.96*std.error,
         color=case_when(lower > 0 ~ "#002F6C",
                         upper < 0 ~ "#BA0C2F",
                         TRUE ~ "#8C8985")
         , term = case_when(term == "info" ~ "Survey information treatment"
                          , term == "D7_USAID" ~ "USAID branded\nactivities"
                          , term == "activities" ~ "Number of USAID activities" 
                          , term == "USAID_message" ~ "Exposed to USAID\nmessaging"
                          )
         )


ggplot(bi.tidy.ords, aes(x=estimate, y=fct_reorder(term, -estimate)), color = color, fill = color) + 
    geom_vline(xintercept=0, color="darkgrey", size=1.2) + 
    geom_errorbarh(aes(xmin=lower, xmax=upper), height=0, size=1, alpha = 0.5) +
    geom_label(aes(label=round(estimate,2), fill = color), size=10, color = "white") +
    scale_fill_manual(values = c("#002F6C")) +
labs(x = "Change in USAID Perception", y = "") + theme(legend.position = "NA", panel.grid.major.y = element_blank())
```
